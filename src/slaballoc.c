#include <stdio.h>
#include <stdlib.h>
#include "slaballoc.h"
#include "objetos.h"
#include <semaphore.h>
#include <pthread.h>

SlabAlloc *crear_cache(char *nombre, size_t tamano_objeto, constructor_fn constructor, destructor_fn destructor){
	SlabAlloc *sla = (SlabAlloc *)malloc(sizeof(SlabAlloc));
	sla->nombre=nombre;
	void* object = (void *)malloc(tamano_objeto*TAMANO_CACHE);
	sla->mem_ptr=object;
	for(int x=0;x<tamano_objeto*TAMANO_CACHE;x=x+tamano_objeto){
		constructor(object+x,tamano_objeto);
}
	sla->mem_ptr=object;
	sla->tamano_objeto=tamano_objeto;
	sla->tamano_cache=TAMANO_CACHE;
	sla->cantidad_en_uso=0;
	Slab* slabsptr=(Slab *)malloc(sizeof(Slab)*TAMANO_CACHE);
	//sla->slab_ptr=(Slab *)malloc(sizeof(Slab)*250);
	for(int n=0;n<TAMANO_CACHE;n++){
		Slab s;
		s.ptr_data=sla->mem_ptr+n*tamano_objeto;
		s.ptr_data_old=NULL;
		s.status=DISPONIBLE;
		sem_init(&(s.sem),0,1);
		*(slabsptr+n)=s;				
	}
	sla->slab_ptr=slabsptr;
	sla->constructor=constructor;
	sla->destructor=destructor;
		
	return sla;
}

void *obtener_cache(SlabAlloc *alloc){
	
	if(alloc != NULL){
		// hay slabs disponibles
		int i;
		for(i=0;i<alloc->tamano_cache;i++){
			sem_wait(&(alloc->slab_ptr[i].sem));
			if(alloc->slab_ptr[i].status == DISPONIBLE){
				alloc->cantidad_en_uso++;
				alloc->slab_ptr[i].status = EN_USO;
				sem_post(&(alloc->slab_ptr[i].sem));
				return 	alloc->slab_ptr[i].ptr_data;
			}
			sem_post(&(alloc->slab_ptr[i].sem));
		}	
	}
	
	return NULL;
}

void devolver_cache(SlabAlloc *alloc, void *obj){
	Slab *s = alloc->slab_ptr;	
	for(int x = 0; x<alloc->tamano_cache; x++){
		if(obj==s->ptr_data+x*alloc->tamano_objeto){
			//si dos usuarios intentan 
			alloc->cantidad_en_uso-=1;
			(*(s+x)).status=0;
			alloc->destructor(alloc->mem_ptr+x*alloc->tamano_objeto,alloc->tamano_objeto);
			
		}
	}	
}

void destruir_cache(SlabAlloc *cache){
	if(cache->cantidad_en_uso==0){
		for(int x = 0; x<cache->tamano_cache;x++){
			cache->destructor(cache->mem_ptr+x*cache->tamano_objeto,cache->tamano_objeto);
}
}
	free(cache->mem_ptr);
	cache->mem_ptr=NULL;
	free(cache->slab_ptr);
	cache->slab_ptr=NULL;
	free(cache);
	cache=NULL;

}
void stats_cache(SlabAlloc *cache){
	printf("Nombre de cache:\t\t %s\n",cache->nombre);
	printf("Cantidad de slabs:\t\t %d\n",cache->tamano_cache);
	printf("Cantidad de slabs en uso:\t %d\n",cache->cantidad_en_uso);
	printf("Tamano de objeto:\t\t %ld\n\n",cache->tamano_objeto);
	
	printf("Direccion de cache->mem_ptr: \t%p\n",cache->mem_ptr);
	Slab* s = cache->slab_ptr;
	for(int n=0;n<cache->tamano_cache;n++){
		printf("Direccion ptr[%d].ptr_data:  %p\t",n,s->ptr_data+n*cache->tamano_objeto);
		if((*(s+n)).status==1) printf("EN_USO,\t");
		else printf("DISPONIBLE,\t");
		printf("ptr[%d].ptr_data_old:\t%p\n",n,s->ptr_data_old+n*cache->tamano_objeto);

	}
}



