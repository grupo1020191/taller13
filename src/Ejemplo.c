#include <unistd.h>
#include <string.h>
#include "objetos.h"

void crear_objeto_ejemplo(void *ref, size_t tamano){
	objeto_ejemplo *obj = (objeto_ejemplo *) ref;
	obj->id_hilo = 0;
	obj->data = malloc(3*sizeof(long));
}

void destruir_objeto_ejemplo(void *ref, size_t tamano){
	objeto_ejemplo *obj = (objeto_ejemplo *) ref;
	obj->id_hilo = 0;
	for(int x = 0; x<3; x++){
		free(obj->data+x);
}
}


