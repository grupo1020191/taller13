#include "../include/slaballoc.h"
#include "../include/objetos.h"
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <string.h>
typedef struct todo{
	SlabAlloc *slaba;
	int indice;
}all;
void* rutina(void* args){
	all *t=(all*)args;
	
	SlabAlloc *slaballoc = (SlabAlloc*)t->slaba;
	int id = t->indice;
	objeto_ejemplo objects[1000];

	for(int x = 0; x < 1000; x++){
	  objeto_ejemplo *object = (objeto_ejemplo *)obtener_cache(slaballoc);
	  object->id_hilo = id;
	  object->data[0] = id*0;
	  object->data[1] = id*1;
	  object->data[2] = id*2;
	  
	  objects[x] = *object;
	}

	for (int x = 700; x < 900; x++) {
	  devolver_cache(slaballoc, &objects[x]);
	}

	for (int x = 0; x < 700; x++) {

	  if (objects[x].id_hilo != id){
	    printf("Error con el id_hilo, valor esperado:  %d\t valor encontrado:  %d\n",id, objects[x].id_hilo);
	  }
	  if (objects[x].data[0] != id*0){
	    printf("Error con el primer valor de *data, valor esperado: %d\t valor encontrado:  %d\n",id*0, objects[x].data[0]);
	  }
	  if (objects[x].data[1] != id*1){
	    printf("Error con el segundo valor de *data, valor esperado: %d\t valor encontrado:  %d\n",id*1, objects[x].data[1]);
	  }
	  if (objects[x].data[2] != id*2){
	    printf("Error con el tercer valor de *data, valor esperado: %d\t valor encontrado:  %d\n\n",id*2, objects[x].data[2]);
	  }
	}
	return NULL;
}


int main(int argc, char **argv){
	if(argc != 3 || strcmp(argv[1],"-n") != 0){
		printf("./bin/proceso -n Numero hilos\n");
		return -1;		
	}
	int numHilos = 0;
	numHilos = atoi(argv[2]);
    	
	SlabAlloc *slaballoc;
	
	
	slaballoc = crear_cache("SlabAlloc",sizeof(objeto_ejemplo),crear_objeto_ejemplo,destruir_objeto_ejemplo);
	all *al = (all*)malloc(sizeof(all));
	al->slaba = slaballoc;
	pthread_t hilos[numHilos];
	memset(hilos,0,numHilos*sizeof(pthread_t));
	for (int x = 0; x < numHilos; x++) {
		pthread_t id;
		al->indice=x;
		pthread_create(&id, NULL,rutina,(void*)al);
		hilos[x] = id;
			
	}
	for (int x = 0;  x< numHilos; x++) {
		pthread_join(hilos[x], NULL);
	}
	
	return 0;
}










