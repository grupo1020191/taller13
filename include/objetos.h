#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
//En este archivo ud podra definir los objetos que podrán ser usados 
//con el slab allocator. Declare tambien el constructor y destructor de 
//cada objeto declarado. Abajo se provee un objeto de ejemplo.
//Declare al menos tres objetos distintos

//El constructor siempre recibe un puntero tipo void al tipo de objeto,
//y su tamaño (tal como se declaro el puntero a funcion).
//No olvide implementar las funciones de crear y destruir de cada objeto.



//TODO: Crear 3 objetos mas

typedef struct objetoEjemplo{
	int id_hilo;
	int *data; //arreglo de 3 enteros
} objeto_ejemplo;

void crear_objeto_ejemplo(void *ref, size_t tamano);

void destruir_objeto_ejemplo(void *ref, size_t tamano);


