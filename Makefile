
bin/prueba: obj/prueba.o obj/Ejemplo.o obj/slaballoc.o
	gcc -pthread obj/prueba.o obj/Ejemplo.o obj/slaballoc.o -o bin/prueba		#agregue los archivos .o que necesite

obj/prueba.o: src/prueba.c
	gcc -Wall -pthread -c -Iinclude/ src/prueba.c -o obj/prueba.o

obj/Ejemplo.o: src/Ejemplo.c
	gcc -Wall -c -Iinclude/ src/Ejemplo.c -o obj/Ejemplo.o
obj/slaballoc.o: src/slaballoc.c
	gcc -Wall -pthread -c -Iinclude/ src/slaballoc.c -o obj/slaballoc.o
#agregue las reglas que necesite


.PHONY: clean
clean:
	rm bin/* obj/*.o

